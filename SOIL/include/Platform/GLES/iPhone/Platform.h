

#ifndef GLES_IPHONE_PLATFORM_H
#define GLES_IPHONE_PLATFORM_H

#include <OpenGLES/ES1/gl.h>
#include <OpenGLES/ES2/gl.h>
#include <OpenGLES/ES2/glext.h>

#define APIENTRY

typedef void (APIENTRY * P_SOIL_GLCOMPRESSEDTEXIMAGE2DPROC) (GLenum target, GLint level, GLenum internalformat, GLsizei width, GLsizei height, GLint border, GLsizei imageSize, const GLvoid * data);

P_SOIL_GLCOMPRESSEDTEXIMAGE2DPROC getComp2DProc()
{
	return glCompressedTexImage2D;
}

#endif	// GLES_IPHONE_PLATFORM_H
