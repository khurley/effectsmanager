

#ifndef GLES_ANDROID_PLATFORM_H
#define GLES_ANDROID_PLATFORM_H

#include <GLES2/gl2.h>

#define APIENTRY

typedef void (APIENTRY * P_SOIL_GLCOMPRESSEDTEXIMAGE2DPROC) (GLenum target, GLint level, GLenum internalformat, GLsizei width, GLsizei height, GLint border, GLsizei imageSize, const GLvoid * data);

P_SOIL_GLCOMPRESSEDTEXIMAGE2DPROC getComp2DProc()
{
	return glCompressedTexImage2D;
}

#endif	// GLES_ANDROID_PLATFORM_H
