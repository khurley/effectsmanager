

#ifndef GL_PLATFORM_H
#define GL_PLATFORM_H

#ifdef WIN32
	#define WIN32_LEAN_AND_MEAN
	#include <windows.h>
	#include <wingdi.h>
	#include <GL/gl.h>
#elif defined(__APPLE__) || defined(__APPLE_CC__)
	/*	I can't test this Apple stuff!	*/
	#include <OpenGL/gl.h>
	#include <Carbon/Carbon.h>
	#define APIENTRY
#else
	#include <GL/gl.h>
	#include <GL/glx.h>
#endif

typedef void (APIENTRY * P_SOIL_GLCOMPRESSEDTEXIMAGE2DPROC) (GLenum target, GLint level, GLenum internalformat, GLsizei width, GLsizei height, GLint border, GLsizei imageSize, const GLvoid * data);

#ifdef WIN32
	P_SOIL_GLCOMPRESSEDTEXIMAGE2DPROC getComp2DProc()
	{
		return (P_SOIL_GLCOMPRESSEDTEXIMAGE2DPROC)
				wglGetProcAddress
				(
					"glCompressedTexImage2DARB"
				);
	}
#elif defined(__APPLE__) || defined(__APPLE_CC__)
	P_SOIL_GLCOMPRESSEDTEXIMAGE2DPROC getComp2DProc()
	{
		P_SOIL_GLCOMPRESSEDTEXIMAGE2DPROC ext_addr;
		/*	I can't test this Apple stuff!	*/
		CFBundleRef bundle;
		CFURLRef bundleURL =
			CFURLCreateWithFileSystemPath(
				kCFAllocatorDefault,
				CFSTR("/System/Library/Frameworks/OpenGL.framework"),
				kCFURLPOSIXPathStyle,
				true );
		CFStringRef extensionName =
			CFStringCreateWithCString(
				kCFAllocatorDefault,
				"glCompressedTexImage2DARB",
				kCFStringEncodingASCII );
		bundle = CFBundleCreate( kCFAllocatorDefault, bundleURL );
		assert( bundle != NULL );
		ext_addr = (P_SOIL_GLCOMPRESSEDTEXIMAGE2DPROC)
				CFBundleGetFunctionPointerForName
				(
					bundle, extensionName
				);
		CFRelease( bundleURL );
		CFRelease( extensionName );
		CFRelease( bundle );
		return ext_addr;
	}
#else
	P_SOIL_GLCOMPRESSEDTEXIMAGE2DPROC getComp2DProc()
	{
		return (P_SOIL_GLCOMPRESSEDTEXIMAGE2DPROC)
		glXGetProcAddressARB
		(
			(const GLubyte *)"glCompressedTexImage2DARB"
		);
	}
#endif

#endif	// GL_PLATFORM_H
