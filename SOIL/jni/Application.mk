
ifdef	NDK_DEBUG
DEBUG		:= 1
CONFIG_DIR	:= Debug
APP_OPTIM 	:= debug
else
CONFIG_DIR	:= Release
APP_OPTIM 	:= release
endif

APP_MODULES := SOIL
NDK_APP_OUT := $(call my-dir)/../libs/Android/$(CONFIG_DIR)
APP_ABI := all