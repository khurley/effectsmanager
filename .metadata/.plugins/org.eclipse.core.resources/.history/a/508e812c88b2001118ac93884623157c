
#ifndef _Effect_H_
#define _Effect_H_ 1

#include <map>
#include <string>

#include "Platform.h"

namespace EffectsManager
{

class Technique;


// Vertex layout description.
typedef struct layoutDesc {
	std::string name; /* name of component (must match in shader pass) */
	GLint components; /* number of components. 1-4 */
	GLenum type;      /* datatype of attribute */
	off_t offset;     /* offset in struct */
} layoutDesc;

// Effect class, a representation of an effect.
class Effect
{
private:
		char *Name;
		typedef std::map<std::string, Technique *> NAMETOTECHNIQUEMAP;
		NAMETOTECHNIQUEMAP m_TechniquesByName;
public:
		Effect(const std::string& filename);
		~Effect();

		/// add a new technique to the effect
		Technique* Addtechnique(const std::string& name);

		/// Get an existing technique by name.
		Technique* GetTechnique(const std::string& name);

		/// return the techniques that are in this effect
		const NAMETOTECHNIQUEMAP *GetTechniques();

		/// Set the vertex layout used by all techniques and passes. If they
		/// don't share the same layout, call SetLayout on them manually.
		/// @param layout
		/// @param stride size of a single vertex (in bytes)
		/// @param n
		/// @return
		int SetLayout(layoutDesc* layout, size_t stride, size_t n);

		/// Test if the effect is valid (it is considered valid if all the techniques and passes
		/// are considered valid.
		bool IsValid() const;
};

}	// namespace EffectsManager

#endif		//_Effect_H_
