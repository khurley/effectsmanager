//
// BasicPath.cpp
// Copyright (c) 2008 - 2012 Charles Baker.  All rights reserved.
//

#include "stdafx.hpp"
#include "path/path.hpp"

#if !defined BUILD_PLATFORM_MSVC && !defined BUILD_PLATFORM_MINGW
#include <unistd.h>
#include <limits.h>
#endif


using namespace sweet::path;

/**
// Get the current working directory.
//
// @return
//  The current working directory.
*/
WidePath sweet::path::current_working_directory()
{
#if defined BUILD_PLATFORM_MSVC || defined BUILD_PLATFORM_MINGW
//
// The length returned by GetCurrentDirectoryW includes the terminating
// null character so the wstring is initialized to have one less character 
// than that.
//   
    DWORD length = ::GetCurrentDirectoryW( 0, NULL );
    std::wstring directory( length - 1, L'\0' );
    ::GetCurrentDirectoryW( length, &directory[0] );
    return WidePath( directory );
#else
    char curDir[PATH_MAX];
    getcwd(curDir, PATH_MAX);
    std::string directory(curDir);
    std::string directoryW;
    directoryW.assign(directory.begin(), directory.end());
    return WidePath( directoryW);
//#error "The function sweet::path::current_working_directory() is not implemented for this platform."
#endif
}
