
#ifndef _EFFECTMANAGER_H_
#define _EFFECTMANAGER_H_

#include "IEffectManager.h"
#include <map>
#include <string>

namespace GLSLEFX
{

/// class: EffectManager
/// description: Manager for Effects loaded from source, Implementation Singleton
///
class EffectManager : public IEffectManager
{
private:
	EffectManager() { }

	typedef std::map<std::string, CREATEOBJECTFUNC> NAMETOOBJMAP;

public:
	virtual ~EffectManager() {};

	virtual int LoadEffectFile(char *source);
	virtual bool SelectTechnique(int effectID, char *name);
	virtual void SetAuxillaryTexture(int effectID, int textureID);

	virtual bool RegisterClass(const char *className, CREATEOBJECTFUNC coFunc);
private:
	friend class IEffectManager;
	static EffectManager *_instance;
	static IEffectManager *GetInstance();
	NAMETOOBJMAP m_NameToObjMap;
};


}

#endif	// _EFFECTMANAGER_H_
