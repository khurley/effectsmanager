//
// IEffectManager:  Interface for Effect Manager
//

namespace EffectsManager
{
class EFObject;

typedef EFObject *(*CREATOBJECTFUNC)();

class IEffectManager
{
private:
    // Dont forget to declare these two. You want to make sure they
    // are unaccessable otherwise you may accidently get copies of
    // your singelton appearing.
	IEffectManager(IEffectManager const&);  // Don't Implement
    void operator=(IEffectManager const&); 	// Don't implement

protected:
	IEffectManager();

public:
	// access to the singleton
	static IEffectManager *GetInstance();

	virtual int LoadEffectFile(char *source) = 0;
	virtual bool SelectTechnique(int effectID, char *name) = 0;
	virtual void SetAuxillaryTexture(int effectID, int textureID) = 0;

	virtual void RegisterObject(char *className, CREATEOBJECTFUNC);

};

}
