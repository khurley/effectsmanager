
#include "IEffectManager.h"
#include "Pass.h"
#include "Uniform.h"
#include <iostream>

namespace GLSLEFX
{

REGISTER_CLASS(Uniform);

Uniform::UNIFORMFUNCINFO Uniform::m_UniformFuncInfos[] = { { false, 1, glUniform1iv }, { false, 2, glUniform2iv }, { false, 3, glUniform3iv }, { false, 4, glUniform4iv },
														{ false, 1, glUniform1fv }, { false, 2, glUniform2fv }, { false, 3, glUniform3fv }, { false, 4, glUniform4fv },
														{ true, 4, glUniformMatrix2fv }, { true, 6, glUniformMatrix2x3fv }, { true, 8, glUniformMatrix2x4fv },
														{ true, 6, glUniformMatrix3x2fv }, { true, 9, glUniformMatrix3fv }, { true, 12, glUniformMatrix3x4fv },
														{ true, 8, glUniformMatrix4x2fv }, { true, 12, glUniformMatrix4x3fv }, { true, 16, glUniformMatrix4fv  }
												};


Uniform::UNIFORMFUNCMAP * Uniform::m_UniformFuncMap = Uniform::MakeUniformFuncInfoMap();

Uniform::Uniform()
{
	m_IsLoaded = false;
	m_UniformFuncInfo = NULL;
}

Uniform::Init(Pass *parent)
{
	EFObject::Init(parent);
	UNIFORMFUNCMAP::iterator ufmIter = m_UniformFuncMap->find(m_Type);
	if (ufmIter != m_UniformFuncMap->end())
	{
		m_UniformFuncInfo = ufmIter->second;
	}

	SyncID(parent->GetProgramID());
}

Uniform::UNIFORMFUNCMAP *Uniform::MakeUniformFuncInfoMap()
{
	static UNIFORMFUNCMAP ufMap;

	for (int i = UNIFORM_TYPE_INT1; i< UNIFORM_TYPE_COUNT; i++)
	{
		ufMap.insert(pair<UNIFORM_TYPE, UNIFORMFUNCINFO *>(i, &m_UniformFuncInfos[i]);
	}
	return &ufMap;
}


Uniform::~Uniform()
{
}

void Uniform::SyncID(GLuint programID)
{
	m_UniformID = glGetUniformLocation(programID, m_UnformName.data());

	if( (int)m_UniformID < 0)
	{
		std::cout << "Warning : shader uniform \"" << name << "\" not found !" << std::endl;
	}
}

void Uniform::Load()
{

	if (m_UniformFuncInfo != NULL)
	{
		if (!m_UniformFuncInfo->m_IsMatrixFunc)
		{
			(*((GLUNIFORMFUNC)(m_UniformFuncInfo->m_UniformFunc)))(m_UnformID, m_UniformFuncInfo->m_NumValues, m_Values);
		}
		else
		{
			(*((GLUNIFORMMATFUNC)m_UniformFuncInfo->m_UniformFunc)))(m_UnformID, m_UniformFuncInfo->m_NumValues, m_Transpose, m_Values);

		}
	}
	m_IsLoaded = true;
}

}
