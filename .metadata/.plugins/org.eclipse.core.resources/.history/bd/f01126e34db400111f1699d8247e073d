//
// IEffectManager:  Interface for Effect Manager
//

#ifndef _IEFFECTMANAGER_H_
#define _IEFFECTMANAGER_H_ 1

#include "EFObject.h"

namespace GLSLEFX
{

#define DECLARE_CLASS(className)																\
private:																						\
	className();																				\
public:																							\
	static EFObject *CreateObject();

#define REGISTER_CLASS(className)																\
	static bool registered_##className = 														\
		IEffectManager::GetInstance()->RegisterClass(#className, &className::CreateObject);		\
		EFObject *className::CreateObject()														\
		{																						\
			return new className();																\
		}

class IEffectManager
{
private:
    // Dont forget to declare these two. You want to make sure they
    // are unaccessable otherwise you may accidently get copies of
    // your singelton appearing.
	IEffectManager(IEffectManager const&);  // Don't Implement
    void operator=(IEffectManager const&); 	// Don't implement

protected:
	IEffectManager() {}

public:
	// access to the singleton
	static IEffectManager *GetInstance();

	virtual int LoadEffectFile(char *source) = 0;
	virtual bool SelectTechnique(int effectID, char *name) = 0;
	virtual void SetAuxillaryTexture(int effectID, int textureID) = 0;
	virtual bool RegisterClass(const char *className, CREATEOBJECTFUNC coFunc) = 0;


};

}

#endif	//_IEFFECTMANAGER_H_
