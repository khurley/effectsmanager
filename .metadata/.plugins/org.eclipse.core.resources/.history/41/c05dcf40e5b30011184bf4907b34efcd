
#ifndef _Technique_H_
#define _Technique_H_ 1

#include "IEffectManager.h"
#include "persist/persist.hpp"
#include <vector>
#include <string>

#include "Platform.h"

namespace GLSLEFX
{

class Pass;

class Technique : public EFObject<Technique>
{
private:
	Technique();
public:
	~Technique();

	typedef std::vector<Pass *> VECPASSES;

	// Initialize the object after object and children are all created
	virtual void Init(EFObject *parent) { }

	/// add a new pass to the technique
	Pass* AddPass(const std::string& name, int passNum);

	/// Get an existing pass by number
	Pass* GetPass(int passNum);

	// grab a vector of passes to go through
	const VECPASSES *GetPasses();

	/// Set the vertex layout used by all techniques and passes. If they
	/// don't share the same layout, call SetLayout on them manually.
	/// @param layout
	/// @param stride size of a single vertex (in bytes)
	/// @param n
	/// @return
	int SetLayout(layoutDesc* layout, size_t stride, size_t n);

	// bind the technique into OpenGL
	void Activate();

	// Tells whenever the effect is valid (it is considered valid if all the techniques and passes
	//are considered valid.
	bool IsValid();
private:
	std::string m_Name;
	// vector of passes for technique
	VECPASSES m_Passes;

	template <class Archive> void persist( Archive& archive )
	 {
		archive.value( "name", m_Name );
	 }
};

}
#endif		// #ifndef _Technique_H_
