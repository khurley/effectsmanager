

#ifndef _PASS_H_
#define _PASS_H_ 1

#include "IEffectManager.h"
#include "persist/persist.hpp"
#include <list>
#include <string>

#include "Platform.h"

namespace EffectsManager
{

// forward reference
class Shader;

class Pass : public EFObject
{
	OBJECT_CLASS(Pass);

public:
	~Pass();

	typedef std::list<Shader *> SHADERLIST;

	/// add a new shader to the pass
	void AddShader(Shader *shader);

	// grab a list of shaders for this pass
	const SHADERLIST *GetShaders();

	void Init(Technique *parent);

	// Tests if the shaders are valid (it is considered valid if the shaders all compile)
	bool IsValid() const;
private:
	std::string m_Name;

	/// list of shaders for this pass
	SHADERLIST m_Shaders;
	/// the program ID from OpenGL
	GLint m_ProgramID;

    template <class Archive> void persist( Archive& archive )
     {
     	archive.value( "name", m_Name );
     }
};

}
