#ifndef _SHADER_H_
#define _SHADER_H_

#include "IEffectManager.h"
#include "persist/persist.hpp"
#include <list>
#include <string>

#include "Platform.h"

using namespace sweet::persist;

namespace GLSLEFX
{

class Uniform;
class Texture;

class Shader : public EFObject
{
private:
	DECLARE_CLASS(Shader);
	typedef std::vector<Uniform *> VECTUNIFORMS;
	typedef std::vector<Texture *> VECTEXTURES;

public:
	virtual ~Shader();

	// Initialize the object after object and children are all created
	virtual void Init(Pass *parent);

	void Activate();
	void Deactivate();

	// shader uniform variables
	void AddUniform(Uniform *uniform);

	// shader texture
	void AddTexture(Texture* Texture);

	GLuint GetShaderiD() { return m_ShaderID; }

private:
	// Load the shader from source
	GLuint LoadShader(GLenum type, const std::string* source);

	VECTUNIFORMS m_Uniforms;
	VECTEXTURES m_Textures;

	GLenum	m_ShaderType;
	std::string m_Source;
	GLuint	m_ShaderID;

    template <class Archive> void persist( Archive& archive )
     {
        static const EnumFilter::Conversion SHADERTYPE[] =
        {
            { GL_FRAGMENT_SHADER, "fragment" },
            { GL_VERTEX_SHADER, "vertex" },
            { GL_GEOMETRY_SHADER, "geometry" },
            { GL_TESS_CONTROL_SHADER, "control" },
            { GL_TESS_EVALUATION_SHADER, "evaluation" },
            { 0, 0 }
        };

        archive.value( "source", m_Source );
        archive.value( "type", m_ShaderType, enum_filter(SHADERTYPE) );
     }

};

}

#endif
