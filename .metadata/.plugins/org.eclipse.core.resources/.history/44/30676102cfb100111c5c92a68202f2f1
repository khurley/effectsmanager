
#ifndef _SMTECHNIQUE_H_
#define _SMTECHNIQUE_H_ 1

#include <map>
#include <string>

namespace EffectsManager
{

using namespace std:
class smPass;


class smTechnique : public smObject
{
	REGISTER_CLASS(smTechnique);
public:
	~smTechnique();

	typedef vector<smPass *> VECPASSES;

	/// add a new pass to the technique
	smPass* AddPass(const std::string& name, int passNum);

	/// Get an existing pass by number
	smPass* GetPass(int passNum);

	// grab a vector of passes to go through
	const VECPASSES *GetPasses();

	/// Set the vertex layout used by all techniques and passes. If they
	/// don't share the same layout, call SetLayout on them manually.
	/// @param layout
	/// @param stride size of a single vertex (in bytes)
	/// @param n
	/// @return
	int SetLayout(layout_desc* layout, size_t stride, size_t n);

	// bind the technique into OpenGL
	void Activate();

	// Tells whenever the effect is valid (it is considered valid if all the techniques and passes
	//are considered valid.
	bool IsValid() const;
private:
		char *Name;
		// vector of passes for technique
		VECPASSES m_Passes;
};

#endif		// #ifndef _SMTECHNIQUE_H_
