#ifndef _SHADER_H_
#define _SHADER_H_

#include "EffectManager.h"
#include <list>
#include <string>

#include "Platform.h"

namespace EffectsManager
{

class Uniform;
class Texture;

class Shader : public emObject
{
	EFFECT_OBJECT(Shader);
private:
	typedef std::vector<Uniform *> VECTUNIFORMS;
	typedef std::vector<Textures *> VECTEXTURES;

public:
	void Init(EFObject *parent);

	void Activate();
	void Deactivate();

	// shader uniform variables
	void addUniform(Uniform *uniform);

	// shader texture
	void addTexture(Texture* Texture);

private:
	// Load the shader from source
	GLuint LoadShader(GLenum type, const std::string* source);

	VECTUNIFORMS m_Uniforms;
	VECTEXTURES m_Textures;

	REGISTER_VALUE(m_ShaderType, "type" , "string");
	std::string *m_ShaderType;
	REGISTER_VALUE(m_Source, "source", "string");
	std::string *m_Source;


};

}

#endif
