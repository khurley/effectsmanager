
#ifndef _EFFECTMANAGER_H_
#define _EFFECTMANAGER_H_

#include "IEffectManager.h"

namespace GLSLEFX
{

/// class: EffectManager
/// description: Manager for Effects loaded from source, Implementation Singleton
///
class EffectManager : public IEffectManager
{
private:
	EffectManager() { }
	friend class IEffectManager;
	static EffectManager *_instance;
	static IEffectManager *GetInstance();

public:
	virtual ~EffectManager() {};
	typedef void *(EFObject::*GETVARIABLEPTR)();

	virtual int LoadEffectFile(char *source);
	virtual bool SelectTechnique(int effectID, char *name);
	virtual void SetAuxillaryTexture(int effectID, int textureID);

	virtual bool RegisterClass(const char *className, CREATEOBJECTFUNC coFunc);

};

}

#endif	// _EFFECTMANAGER_H_
