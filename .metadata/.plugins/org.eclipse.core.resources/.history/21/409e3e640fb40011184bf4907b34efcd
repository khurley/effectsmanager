
#include "IEffectManager.h"
#include "Pass.h"
#include "Texture.h"

namespace GLSLEFX
{

REGISTER_CLASS(Texture);


Texture::Texture()
{
	glGenTextures(1, &m_TexID);

	// default clamp modes = OpenGL default modes
	m_sClampMode = GL_CLAMP_TO_EDGE;
	m_tClampMode = GL_CLAMP_TO_EDGE;
	m_rClampMode = GL_CLAMP_TO_EDGE;

	// default minification filter modes = OpenGL default filter modes
	m_MinFilter = GL_LINEAR;
	// default magnitification filter modes = OpenGL default filter modes
	m_MagFilter = GL_LINEAR;
}

void Texture::Init(Shader *parent)
{
	EFObject::Init(parent);

}

void Texture::SetActive()
{
	glActiveTexture(m_TextureUnit);
}

void Texture::Bind()
{
	glBindTexture(m_GLTextureType, m_TexID);

	glTexParameteri(m_GLTextureType, GL_TEXTURE_WRAP_S, m_sClampMode);
	if (m_GLTextureType != GL_TEXTURE_1D)
	{
		glTexParameteri(m_GLTextureType, GL_TEXTURE_WRAP_T, m_tClampMode);
	}

	if (m_GLTextureType == GL_TEXTURE_3D)
	{
		glTexParameteri(m_GLTextureType, GL_TEXTURE_WRAP_R, m_rClampMode);
	}

	glTexParameteri(m_GLTextureType, GL_TEXTURE_MIN_FILTER, m_MinFilter);
	glTexParameteri(m_GLTextureType, GL_TEXTURE_MAG_FILTER, m_MagFilter);

}

void Texture::Unbind()
{
	glBindTexture(m_GLTextureType, 0);
}


void Texture::Load(GLuint xDim, GLuint nLods, void *data, GLuint internalFormat, GLuint format, GLuint type)
{
	assert(m_GLTextureType == GL_TEXTURE_1D);

	SetActive();
	Bind();


	GLuint s=1;
	for(GLuint i=0 ; i < nLods ; ++i)
	{
		glTexImage1D( m_GLTextureType, i, internalFormat, xDim, 0, format, type, data[i]);
		CheckGLError("Texture1D::load");
	}
}



void Texture::Load(GLuint xDim, GLuint yDim, GLuint nLods, void **data, GLuint internalFormat, GLuint format, GLuint type)
{
	assert(m_GLTextureType == GL_TEXTURE_2D);

	SetActive();
	Bind();


	GLuint s=1;
	for(GLuint i=0 ; i < nLods ; ++i)
	{
		glTexImage2D( m_GLTextureType, i, internalFormat, xDim, yDim, 0, format, type, data[i]);
		CheckGLError("Texture2D::load");
	}
}

void Texture::Load(GLuint xDim, GLuint yDim, GLuint zDim, GLuint nLods, void **data, GLuint internalFormat, GLuint format, GLuint type)
{
	assert(m_GLTextureType == GL_TEXTURE_2D);

	SetActive();
	Bind();


	GLuint s=1;
	for(GLuint i=0 ; i < nLods ; ++i)
	{
		glTexImage3D( m_GLTextureType, i, internalFormat, xDim, yDim, zDim, 0, format, type, data[i]);
		CheckGLError("Texture3D::load");
	}
}
}
