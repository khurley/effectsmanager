#ifndef _UNIFORM_H_
#define _UNIFORM_H_

#include "IEffectManager.h"
#include "persist/persist.hpp"
#include "Platform.h"
#include <string>

namespace EffectsManager
{

class Uniform : public EFObject
{
private:
	Uniform();

private;
	typedef enum UNIFORM_TYPE
	{
		UNIFORM_TYPE_INT1;
		UNIFORM_TYPE_INT2;
		UNIFORM_TYPE_INT3;
		UNIFORM_TYPE_INT4;
		UNIFORM_TYPE_FLOAT1;
		UNIFORM_TYPE_FLOAT2;
		UNIFORM_TYPE_FLOAT3;
		UNIFORM_TYPE_FLOAT4;
		UNIFORM_TYPE_MATRIX2X2;
		UNIFORM_TYPE_MATRIX2X3;
		UNIFORM_TYPE_MATRIX2X4;
		UNIFORM_TYPE_MATRIX3X2;
		UNIFORM_TYPE_MATRIX3X3;
		UNIFORM_TYPE_MATRIX3X4;
		UNIFORM_TYPE_MATRIX4X2;
		UNIFORM_TYPE_MATRIX4X3;
		UNIFORM_TYPE_MATRIX4X4;
		UNIFORM_TYPE_COUNT
	} UNIFORM_TYPE;

	typedef void (*GLUNIFORMMATFUNC)(GLint  location,  GLsizei  count,  GLboolean  transpose,  const GLfloat *values);
	typedef void (*GLUNIFORMFUNC)(GLint locataion, GLsizei count,  const void *values);

public:

	// Initialize the object after object and children are all created
	virtual void Init(EFObject *parent) { }

	/// Sync the ID with the uniform by grabbing the ID and location from compiled shader
	void SyncID();
	/// load the uniform into the memory for shader access
	void Load();

private:
	GLuint m_UniformID;						/// the ID returned from OpenGL
	bool m_IsLoaded;						/// is it loaded into the shader memory?
	GLUNIFORMMATFUNC m_UniformMatrixFunc;	/// Uniform matrix setting/loading function pointer
	GLUNIFORMFUNC m_UniformFunc;			/// Uniform setting/loading function pointer
	union {
		GLint	m_IntValues[4];					/// uniform with integer values
		GLflat	m_FloatValues[16];				/// array of floats max 4x4 matrix
	} m_Values;
	std::string m_Name;
	UNIFORM_TYPE m_Type;					/// the type of uniform
	bool m_MatTranspose;					/// if the matrix should be transposed

public:
	template <class Archive> void persist( Archive& archive )
	 {
		static const EnumFilter::Conversion UNIFORM_TYPES[] =
		{
			{ UNIFORM_TYPE_INT1, "int1" },
			{ UNIFORM_TYPE_INT2, "int2" },
			{ UNIFORM_TYPE_INT3, "int3" },
			{ UNIFORM_TYPE_INT4, "int4" },
			{ UNIFORM_TYPE_FLOAT1, "float1" },
			{ UNIFORM_TYPE_FLOAT2, "float2" },
			{ UNIFORM_TYPE_FLOAT3, "float3" },
			{ UNIFORM_TYPE_FLOAT4, "float4" },
			{ UNIFORM_TYPE_MATRIX2X2, "matrix2x2" },
			{ UNIFORM_TYPE_MATRIX2X3, "matrix2x3" },
			{ UNIFORM_TYPE_MATRIX2X4, "matrix2x4" },
			{ UNIFORM_TYPE_MATRIX3X2, "matrix3x2" },
			{ UNIFORM_TYPE_MATRIX3X3, "matrix3x3" },
			{ UNIFORM_TYPE_MATRIX3X4, "matrix3x4" },
			{ UNIFORM_TYPE_MATRIX4X2, "matrix4x2" },
			{ UNIFORM_TYPE_MATRIX4X3, "matrix4x3" },
			{ UNIFORM_TYPE_MATRIX4X4, "matrix4x4" },
			{ 0, 0 }
		};

		archive.value( "name", m_Name );
		archive.value( "type", m_type, enum_filter(UNIFORM_TYPES) );
		archive.value( "transpose", m_MatTranspose );
		archive.value( "values", m_Values);
	 }
};

}

#endif	// #ifndef _UNIFORM_H_

