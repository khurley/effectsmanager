//
// IEffectManager:  Interface for Effect Manager
//

namespace EffectsManager
{

#include "EFObject.h"

#define REGISTER_CLASS(className)																\
	static int register##className = 														\
		IEffectManager::GetInstance()->RegisterObject(#className, ##className::CreateObject);	\

#define EFFECT_OBJECT(className)																\
private:																						\
		className();																			\
public:																							\
		static EFObject *CreateObject()															\
		{																						\
			return new className();																\
		}



#define EFFECT_VARIABLE(type, name)																\
public:																							\
		void *get_##name() { return &##name);													\
private:																						\
		##type ##name;

#define REGISTER_VARIABLE(variableName, valueName, valueType)									\
		static int register##variableName = 													\
			IEffectManger::GetInstance()->RegisterVariable(valueName, valueType, 				\
			get_##variableName )

typedef EFObject *(EFObject::*CREATEOBJECTFUNC)();

class IEffectManager
{
private:
    // Dont forget to declare these two. You want to make sure they
    // are unaccessable otherwise you may accidently get copies of
    // your singelton appearing.
	IEffectManager(IEffectManager const&);  // Don't Implement
    void operator=(IEffectManager const&); 	// Don't implement

protected:
	IEffectManager();

public:
	// access to the singleton
	static IEffectManager *GetInstance();

	virtual int LoadEffectFile(char *source) = 0;
	virtual bool SelectTechnique(int effectID, char *name) = 0;
	virtual void SetAuxillaryTexture(int effectID, int textureID) = 0;
	virtual void RegisterObject(char *className, CREATEOBJECTFUNC coFunc) = 0;
	virtual void RegisterVariable(char *valueName, char *valueType, GETVARIABLEPTR getVarPtr) = 0;

};

}
