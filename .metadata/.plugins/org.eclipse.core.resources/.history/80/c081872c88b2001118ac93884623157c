#ifndef _TEXTURE_H_
#define _TEXTURE_H_

#include "EffectManager.h"
#include "Platform.h"

namespace EffectsManager
{

class Texture : emObject
{
	REGISTER_CLASS(Texture);
public:
	Texture(int nDimensions, GLuint textureUnit);
	/// Sets the
	void SetActive();
	/// Bind the texture in OpenGL
	void Bind();
	/// Unbind the texture in OpenGL
	void Unbind();

	/// set the Clamp modes for the texture unit
	void SetClampModes(GLenum sClampMode, GLenum tClampMode = -1, GLenum rClampMode = -1);
	/// get the Clamp modes for the texture unit
	void GetClampModes(GLenum *sClampMode, GLenum *tClampMode = NULL, GLenum *rClampMode = NULL);

	/// Set the minification/magnifications type for the texture unit
	void SetMinMagModes(GLenum minMode, GLenum magMode);
	void GetMinMagModes(GLenum *minMode = NULL, GLenum *magMode = NULL);

private:
	GLuint m_TexID;				// the GL texture ID allocated for this texture
	GLenum m_GLTextureType;		/// the GL texture type GL_TEXTURE_1D, GL_TEXTURE_2D, GL_TEXTURE_3D
	REGISTER_VALUE(m_TextureUnit, "unit" , "int");
	GLuint m_TextureUnit;		// the Texture Unit (Sampler) unit to assign texture to.
	REGISTER_VALUE(m_TextureType, "type", "string")
	std::string m_TextureType;
};

}

#endif
