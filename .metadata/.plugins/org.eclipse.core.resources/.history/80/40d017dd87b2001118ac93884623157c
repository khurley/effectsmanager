
#ifndef _EFFECTMANAGER_H_
#define _EFFECTMANAGER_H_

#include "IEffectManager.h"
#include "EFObject.h"

namespace EffectsManager
{

typedef EFObject *(EFObject::*CREATEOBJECTFUNC)();

/// class: EffectManager
/// description: Manager for Effects loaded from source, Implementation Singleton
///
class EffectManager : public IEffectManager
{
private:
	EffectManager() { }
	static EffectManager _instance;
public:
	virtua ~EffectManager() {};
	typedef void *(EFObject::*GETVARIABLEPTR)();

	virtual int LoadEffectFile(char *source);
	virtual bool SelectTechnique(int effectID, char *name);
	virtual void SetAuxillaryTexture(int effectID, int textureID);

	virtual void RegisterObject(char *className, CREATEOBJECTFUNC coFunc);
	virtual void RegisterVariable(char *valueName, char *valueType, GETVARIABLEPTR getVarPtr);

};

}

#endif	// _EFFECTMANAGER_H_
