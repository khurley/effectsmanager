#ifndef _TEXTURE_H_
#define _TEXTURE_H_

#include "IEffectManager.h"
#include "persist/persist.hpp"
#include "Platform.h"

namespace GLSLEFX
{

using namespace sweet::persist;

class Pass;

class Texture : EFObject
{
private:
	DECLARE_CLASS(Texture);

public:
	// Initialize the object after object and children are all created
	virtual void Init(Pass *parent);

	/// Sets the
	void SetActive();
	/// Bind the texture in OpenGL
	void Bind();
	/// Unbind the texture in OpenGL
	void Unbind();

	/// set the Clamp modes for the texture unit
	void SetClampModes(GLenum sClampMode, GLenum tClampMode = -1, GLenum rClampMode = -1);
	/// get the Clamp modes for the texture unit
	void GetClampModes(GLenum *sClampMode, GLenum *tClampMode = NULL, GLenum *rClampMode = NULL);

	/// Set the minification/magnifications type for the texture unit
	void SetMinMagModes(GLenum minMode, GLenum magMode);
	void GetMinMagModes(GLenum *minMode = NULL, GLenum *magMode = NULL);

private:
	GLuint m_TexID;				// the GL texture ID allocated for this texture
	GLenum m_GLTextureType;		/// the GL texture type GL_TEXTURE_1D, GL_TEXTURE_2D, GL_TEXTURE_3D
	GLuint m_TextureUnit;
	GLenum m_sClampMode;		/// the clamp mode for s component of texture
	GLenum m_tClampMode;		/// the clamp mode for s component of texture
	GLenum m_rClampMode;		/// the clamp mode for s component of texture
	GLenum m_MinMode;			/// the minification type
	GLenum m_MagMode;			/// the magnification type

    template <class Archive> void persist( Archive& archive )
     {
    	static const EnumFilter::Conversion TEXTURETYPE[] =
        {
            { GL_TEXTURE_1D, "1d" },
            { GL_TEXTURE_2D, "2d" },
            { GL_TEXTURE_3D, "3d" },
            { 0, 0 }
        };

    	static const EnumFilter::Conversion CLAMPMODETYPE[] =
        {
            { GL_NEAREST, "nearest" },
            { GL_LINEAR, "linear" },
            { GL_NEAREST_MIPMAP_NEAREST, "near_mip_near" },
            { GL_LINEAR_MIPMAP_NEAREST, "linear_mip_near" },
            { GL_NEAREST_MIPMAP_LINEAR, "near_mip_linear" },
            { GL_LINEAR_MIPMAP_LINEAR, "linear_mip_linear" },
            { 0, 0 }
        };

		static const EnumFilter::Conversion MINMAGTYPE[] =
		{
			{ GL_LINEAR, "linear" },
			{ GL_NEAREST, "nearest" },
			{ 0, 0 }
		};


        archive.value( "sampler", m_TextureUnit );
        archive.value( "type", m_GLTextureType, enum_filter(TEXTURETYPE) );
        archive.value( "s_clampmode", m_sClampMode, enum_filter(CLAMPMODETYPE) );
        archive.value( "t_clampmode", m_tClampMode, enum_filter(CLAMPMODETYPE) );
        archive.value( "r_clampmode", m_rClampMode, enum_filter(CLAMPMODETYPE) );
        archive.value( "minmode", m_MinMode, enum_filter(MINMAGTYPE) );
        archive.value( "magmode", m_MagMode, enum_filter(MINMAGTYPE) );
     }
};

}

#endif
