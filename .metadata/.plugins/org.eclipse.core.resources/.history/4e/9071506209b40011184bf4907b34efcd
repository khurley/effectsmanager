
#include "IEffectManager.h"
#include "Texture.h"

namespace GLSLEFX
{

REGISTER_CLASS(Texture);


Texture::Texture()
{
	glGenTextures(1,m_TexID);

	// default clamp modes = OpenGL default modes
	m_sClampMode = GL_CLAMP_TO_EDGE;
	m_tClampMode = GL_CLAMP_TO_EDGE;
	m_rClampMode = GL_CLAMP_TO_EDGE;

	// default minification filter modes = OpenGL default filter modes
	m_sMinFilter = GL_LINEAR;
	m_tMinFilter = GL_LINEAR;
	m_rMinFilter = GL_LINEAR;

	// default magnitification filter modes = OpenGL default filter modes
	m_sMaxFilter = GL_LINEAR;
	m_tMaxFilter = GL_LINEAR;
	m_rMaxFilter = GL_LINEAR;
}

void Texture::Init(Shader *parent)
{
	EFObject::Init(parent);
}

void Texture::SetActive()
{
	glActiveTexture(m_TextureUnit);
}

void Texture::Bind()
{
	glBindTexture(m_GLTextureType, m_TexID);
}

void Texture::Unbind()
{
	glBindTexture(m_GLTextureType, 0);
}

void Texture::SetClampMode(GLenum sClampMode, GLenum tClampMode, GLenum rClampMode)
{
	SetActive();
	Bind();

	m_sClampMode = sClampMode;
	glTexParameteri(m_GLTextureType, GL_TEXTURE_WRAP_S, sClampMode);
	CheckGLError("Texture::SetClampMode");
	if (tClampMode != -1)
	{
		m_tClampMode = tClampMode;
		glTexParameteri(m_GLTextureType, GL_TEXTURE_WRAP_T, tClampMode);
		CheckGLError("Texture::SetClampMode");
	}

	if (rClampMode != -1)
	{
		m_rClampMode = rClampMode;
		glTexParameteri(m_GLTextureType, GL_TEXTURE_WRAP_R, rClampMode);
		CheckGLError("Texture::SetClampMode");
	}
}

void Texture::GetClampModes(GLenum *sClampMode, GLenum *tClampMode, GLenum *rClampMode)
{
	if (*sClampMode != NULL)
	{
		*sClampMode = m_sClampMode;
	}

	if (*tClampMode != NULL)
	{
		*tClampMode = m_tClampMode;
	}

	if (*rClampMode != NULL)
	{
		*rClampMode = m_rClampMode;
	}
}

void Texture::SetMinMagModes(GLenum minMode, GLenum magMode)
{
	SetActive();
	Bind();

	glTexParameteri(m_GLTextureType, GL_TEXTURE_MIN_FILTER, minMode);
	CheckGLError("Texture::SetMinMagMode::min");

	glTexParameteri(m_GLTextureType, GL_TEXTURE_MAG_FILTER, magMode);
	CheckGLError("Texture::SetMinMagMode::mag");
}

void Texture::GetMinMagModes(GLenum *minMode, GLenum *magMode)
{
	if (*minMode != NULL)
	{
		*minMode = m_MinMode;
	}

	if (*magMode != NULL)
	{
		*magMode = m_MagMode;
	}
}

void Load(GLuint xDim, GLuint nLods, void *data, GLuint internalFormat, GLuint format, GLuint type, bool linear, bool clamp)
{
	assert(m_GLTextureType == GL_TEXTURE_1D);

	SetActive();
	Bind();


	GLuint s=1;
	for(GLuint i=0 ; i < nLods ; ++i)
	{
		glTexImage1D( m_GLTextureType, i, internalFormat, xDim, 0, format, type, data[i]);
		CheckGLError("Texture1D::load");
	}
}



void Texture::Load(GLuint xDim, GLuint yDim, GLuint nLods, void **data, GLuint internalFormat, GLuint format, GLuint type, bool linear, bool clamp)
{
	assert(m_GLTextureType == GL_TEXTURE_2D);

	SetActive();
	Bind();


	GLuint s=1;
	for(GLuint i=0 ; i < nLods ; ++i)
	{
		glTexImage2D( m_GLTextureType, i, internalFormat, xDim, yDim, 0, format, type, data[i]);
		CheckGLError("Texture2D::load");
	}
}

void Texture::Load(GLuint xDim, GLuint yDim, GLuint zDim, GLuint nLods, void **data, GLuint internalFormat, GLuint format, GLuint type, bool linear, bool clamp)
{
	assert(m_GLTextureType == GL_TEXTURE_2D);

	SetActive();
	Bind();


	GLuint s=1;
	for(GLuint i=0 ; i < nLods ; ++i)
	{
		glTexImage3D( m_GLTextureType, i, internalFormat, xDim, yDim, zDim, 0, format, type, data[i]);
		CheckGLError("Texture3D::load");
	}
}
}
