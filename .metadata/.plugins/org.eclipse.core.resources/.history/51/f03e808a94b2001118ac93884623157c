
#ifndef _EFOBJECT_H_
#define _EFOBJECT_H_


namespace EffectsManager
{
#define EFFECT_OBJECT(className)																\
private:																						\
		className();																			\
public:																							\
		static EFObject *CreateObject()															\
		{																						\
			return new className();																\
		}																						\
		#undef CURRENTCLASS																		\
		#define CURRENTCLASS ##className

#define EFFECT_OBJECT_VAR(type, name)												\
public:																							\
		static void *get_##name(EFOject *obj) { return &((CURRENTCLASS *)obj)->##name);			\
private:																						\
		##type ##name;

class EFObject
{
private:
	EFObject();

public:
	typedef void *(EFObject::*EFO_VOIDPTR);
	// virtual destructor to make sure children get called
	virtual ~EFObject() {}

	// Initialize the object after object and children are all created
	virtual void Init(EFObject *parent) = 0;
	// only allow call of CreateObject to create the class
	virtual EFObject *CreateObject() = 0;
};

}

#endif	// #ifndef _EFOBJECT_H_
